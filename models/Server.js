const express = require("express");
const cors = require('cors');
const { DB } = require("../database/config");


class Server {
  constructor() {
    this.app = express();
    this.port = process.env.PORT;
    this.usuariosRoutePath = '/api/usuarios';
    this.AuthPath = '/api/auth';
    
    //Middlewares
    this.middlewares();

    this.connectToDb();

    this.routes();
  }

  middlewares() {
      //Directorio publico
      this.app.use(express.static('public'));
      this.app.use(cors());
      //Lectura y Parseo de datos enviados a backend
      this.app.use(express.json())
  }

  routes() {
    this.app.use(this.usuariosRoutePath, require('../routes/user'))
    this.app.use(this.AuthPath, require('../routes/auth'))
  }

  listen() {
      this.app.listen(this.port , () => {
          console.log("listening on port " + this.port);
      })
  }

  async connectToDb() {
    await DB()
  }
}

module.exports = Server;
