const Rol = require("../models/Rol");
const Usuario = require("../models/Usuario");

const esRolValido = async (rol = "") => {
  const existeRol = await Rol.findOne({ rol });

  if (!existeRol) throw new Error(`el rol ${rol} no existe en la DB`);
};

const emailExiste = async (correo) => {
  const existeEmail = await Usuario.findOne({ correo });

  if (existeEmail) throw new Error(`el correo ${correo} ya esta registrado`);
};

const existeUsuarioPorId = async(id) => {
    const existeUsuario = await Usuario.findById(id);

    if (!existeUsuario) throw new Error(`el usuario ${id} no existe`);
}

module.exports = {
  esRolValido,
  emailExiste,
  existeUsuarioPorId
};
