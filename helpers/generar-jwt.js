const jwt = require("jsonwebtoken");

const generarJWT = (uid) => {
  return new Promise((resolve, reject) => {
    const publicKey = process.env.PUBLIC_SECRET_KEY;

    const payload = {
      uid,
    };

    jwt.sign(payload, publicKey, {
        expiresIn: '4H'
    }, (err, token) => {
        if(err) {
            console.log(err);
            reject('',err)
        } else {
            resolve(token)
        }

    });
  });
};

module.exports = {
  generarJWT,
};
