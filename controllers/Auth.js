const { response } = require("express");
const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");
const { generarJWT } = require("../helpers/generar-jwt");
const { verifyGoogleToken } = require("../helpers/google-verify");
const { json } = require("express/lib/response");

const AuthLoginPost = async (req, res = response) => {
  const { correo, password } = req.body;

  try {
    //verificar si email existe
    const usuario = await Usuario.findOne({ correo });
    if (!usuario) return res.status(404).json({ msg: "Usuario inexistente" });

    //verificar si usuario esta activo
    if (!usuario.estado)
      res.status(404).json({ msg: "El usuario no esta activo" });

    //verificar la contrasenia
    const validPassword = bcryptjs.compareSync(password, usuario.password);

    if (!validPassword) res.status(404).json({ msg: "credenciales invalidas" });

    //generar JWT
    const jwt = await generarJWT(usuario.id);

    res.json({ usuario, token: jwt });
  } catch (error) {
    console.log(error);
    res.status(500).json({
      msg: "Hable con el administrador",
    });
  }
};

const AuthGooglePost = async (req, res = response) => {
  const { id_token } = req.body;

  try {
    const { email, given_name, family_name } = await verifyGoogleToken(
      id_token
    );

    let usuario = await Usuario.findOne({correo:email});

    if(!usuario) {
      const dataUser = {
        correo: email, 
        nombre: given_name,
        apellido: family_name,
        password: ':p',
        google: true,
        rol:'USER_ROLE'
      }

      usuario = new Usuario(dataUser)
      await usuario.save();
    } 


    if (!usuario.estado) {
      return res.status(401).json({
        msg: 'Hable con el administrador, usuario bloqueado'
      })
    }



    //generar JWT
    const jwt = await generarJWT(usuario.id);

    res.json({ usuario, token: jwt });


  } catch (error) {
    console.log(error)
    res.status(400).json({
      ok: false,
      msg: "El token no se pudo verificar",
    });
  }
};

module.exports = {
  AuthLoginPost,
  AuthGooglePost,
};
