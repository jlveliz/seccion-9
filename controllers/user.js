const { response } = require("express");
const Usuario = require("../models/Usuario");
const bcryptjs = require("bcryptjs");

const UsuariosGet = async (req, res = response) => {
  const { limite = 5, desde = 0 } = req.query;

 

  const [total, usuarios] = await Promise.all([
    Usuario.countDocuments({estado: true}),
    Usuario.find({estado: true})
    .limit(Number(limite))
    .skip(Number(desde))

  ]);

  res.json({total,usuarios});
};

const UsuarioGet = (req, res = response) => {
  const params = req.query;

  res.json({
    msg: "get API - Controller",
    params,
  });
};

const UsuarioPost = async (req, res = response) => {
  const { nombre, correo, password, rol } = req.body;
  const usuario = new Usuario({ nombre, correo, password, rol });

  //Encriptar el password
  const salt = bcryptjs.genSaltSync();
  usuario.password = bcryptjs.hashSync(password, salt);

  //Guardar en DB
  await usuario.save();

  res.json({
    usuario,
  });
};
const UsuarioPut = async (req, res = response) => {
  const { id } = req.params;
  const { password, google, correo, ...user } = req.body;

  //TODO VALIDAR CONTRA LA DB

  if (password) {
    //Encriptar el password
    const salt = bcryptjs.genSaltSync();
    user.password = bcryptjs.hashSync(password, salt);
  }

  const usuarioDB = await Usuario.findByIdAndUpdate(id, user);

  res.json({ usuarioDB });
};
const UsuarioDelete = async (req, res = response) => {

  const {id} = req.params;

  //fisicamente lo borramos
  // const usuario = await Usuario.findByIdAndDelete(id);

  const usuario = await Usuario.findByIdAndUpdate(id, {estado : false});


  res.json(usuario);
};

module.exports = {
  UsuariosGet,
  UsuarioGet,
  UsuarioDelete,
  UsuarioPost,
  UsuarioPut,
};
