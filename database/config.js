const mongoose = require("mongoose");

const DB = async () => {
  try {
    await mongoose.connect(process.env.MONGO_DB, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });

    console.log("Conectqado a la base de datos")
  } catch (error) {
      console.log(error)
      throw new Error(error)
  }
};

module.exports= {
    DB
}
