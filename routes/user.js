const { Router } = require("express");
const { check } = require("express-validator");
const {
  UsuariosGet,
  UsuarioGet,
  UsuarioPost,
  UsuarioPut,
  UsuarioDelete,
} = require("../controllers/user");
const {
  esRolValido,
  emailExiste,
  existeUsuarioPorId,
} = require("../helpers/db-validators");
const { validarJwt } = require("../middlewares/valida-jwt");
const { esAdmin, tieneRole } = require("../middlewares/valida-roles");
const ValidarCampos = require("../middlewares/ValidarCampos");
// const { 
//   ValidarCampos,
//   validarJwt,
//   tieneRole
// } = require('../middlewares')


const router = Router();

router.get("/", UsuariosGet);

router.get("/:id", UsuarioGet);

router.put(
  "/:id",
  [
    check("id", "No es un Id valido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    check("rol").custom(esRolValido),
    ValidarCampos,
  ],
  UsuarioPut
);

router.post(
  "/",
  [
    check("nombre", "el nombre es requerido").notEmpty(),
    check(
      "password",
      "El password debe ser obligatorio, y debe tener 6 caracteres"
    )
      .isLength(6)
      .notEmpty(),
    check("correo", "No es un correo valido").isEmail(),
    // check("rol", "No es un rol valido").isIn(["ADMIN_ROLE", "USER_ROLE"]),
    check("correo").custom(emailExiste),
    check("rol").custom(esRolValido),
    ValidarCampos,
  ],
  UsuarioPost
);

router.delete(
  "/:id",
  [
    validarJwt,
    // esAdmin,
    tieneRole("ADMIN_ROLE","VENTA_ROLE"),
    check("id", "No es un Id valido").isMongoId(),
    check("id").custom(existeUsuarioPorId),
    ValidarCampos,
  ],
  UsuarioDelete
);

module.exports = router;
