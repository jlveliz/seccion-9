const { Router } = require("express");
const { check } = require("express-validator");
const { AuthLoginPost, AuthGooglePost } = require("../controllers/Auth");
const ValidarCampos = require("../middlewares/ValidarCampos");

const router = Router();

router.post(
  "/login",
  [
    check("correo", "correo invalido").isEmail(),
    check(
      "password",
      "El password debe ser obligatorio"
    ).notEmpty(),
    ValidarCampos,
  ],
  AuthLoginPost
);

router.post(
  "/google",
  [
    check("id_token", "El Id Token de Google es necesario").notEmpty(),
    ValidarCampos,
  ],
  AuthGooglePost
);

module.exports = router;
