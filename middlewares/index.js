const  validaJwt  = require("../middlewares/valida-jwt");
const validaRoles = require("../middlewares/valida-roles");
const {ValidarCampos} = require("../middlewares/ValidarCampos");

module.exports = {
    ...ValidarCampos,
    ...validaRoles,
    ...validaJwt
}